#!/usr/bin/env python3
# W.J. van der Laan 2017, distributed under MIT license
# James Hilliard 2017
import binascii
import base64
import json
import os, sys
import struct
from Crypto import Random
from Crypto.Cipher import AES
from pprint import pprint

KEY = binascii.a2b_hex(b'fffffbffeffffbfffbbfffbfdbfff7ffffffffffffffdfffff7fffffbfffffff')

def pad(s):
    '''PKCS7 pad with crc prefix.'''
    bytestring = bytes(s, 'utf-8')
    crc = struct.pack('I',binascii.crc32(bytestring))
    bytestring = crc + bytestring
    k=16
    l = len(bytestring)
    val = k - (l % k)
    return bytestring + bytes([val] * val)

def encrypt(data_in):
    # first 32 bytes are IV, we only need 16 of that
    iv = Random.new().read(16)
    cipher = AES.new(KEY, AES.MODE_CBC, iv)
    data_out = pad(data_in)
    data_out = cipher.encrypt(data_out)
    data_out = iv + Random.new().read(16) + data_out
    data_out = base64.b64encode(data_out)
    return data_out

def main():
    if len(sys.argv) < 2:
        print('Usage: %s /path/to/configfile.json' % os.path.basename(sys.argv[0]))
        exit(1)
    with open(sys.argv[1]) as f:
        data_in = json.load(f)

    out_recs = []
    for record in data_in:
        recordstr = json.dumps(data_in[record], separators=(',', ':'))
        encdata = encrypt(recordstr).decode('utf-8')
        out_recs.append({'data': encdata, 'type': int(record)})
    out_recsstr = json.dumps(out_recs, separators=(',', ':'))
    out_data = encrypt(out_recsstr)

    if len(sys.argv) == 3:
        with open(sys.argv[2], 'w') as outfile:
            outfile.write(out_data.decode('utf-8'))
    else:
        print(out_data)


if __name__ == '__main__':
    main()